## Arranque del sistema:   
Dos familias : la familia de los init / rc (old)
y otra familia systemd todos estos procesos tienen el PID=1  

init consisteix en una llista de pasos seqüencials   

systemd el que fa es fer una arrancada concurrent.  hi ha dependencies  
el que fa es:  
-POST (power on self test)   
-SETUP (diu d'on ha d'arrencar , HD,DVD,USB...)  
busca per arrancar en el primer disc   
-(MBR) GRUB (menu amb una serie d'opcions)
       En el grub hi ha un kernel y initramfs  
       en el kernel li podem pasar opcions
opcions kernel:
- root = (device de la partició arrel)
- ro   
- quiet (tot alló que no veiem de les ordres d'arrencada o podem veure amb l'ordre `dmesg`)
- systemd.unit = (target)  
- si no diem cual , arranca amb el default.target

el kernel el que fa es 'anar' a un target  
- engegar 'units' (serveis)  

per fer els targets e iniciar totes aquestes coses es necessita els Vnits  
- targets  
- services 
- sockets  
- mounts  




# Systemctl :

systemctl get-default 
systemctl set-default multi-user.target  
systemctl list-dependencies (mostra totes les dependencies del default.target si no indiquem quin target)  
systemctl list-dependencies basic.target  
```bash
systemctl list-dependencies | grep "target" # tots els targets
default.target
● ├─basic.target
● │ ├─paths.target
● │ ├─slices.target
● │ ├─sockets.target
● │ ├─sysinit.target
● │ │ ├─cryptsetup.target
● │ │ ├─integritysetup.target
● │ │ ├─local-fs.target
● │ │ ├─swap.target
● │ │ └─veritysetup.target
● │ └─timers.target
● ├─getty.target
● └─remote-fs.target
```  

systemctl list-unit-files # llista totes les unit que tenim instalades , 
                          # tant si estan funcionant com si no 

les Vnits estan en /lib/systemd/system   


```bash
root@dminimal:~# systemctl list-unit-files --type socket
UNIT FILE                        STATE    PRESET 
dbus.socket                      static   -      
ssh.socket                       disabled enabled
syslog.socket                    static   -      
systemd-fsckd.socket             static   -      
systemd-initctl.socket           static   -      
systemd-journald-audit.socket    static   -      
systemd-journald-dev-log.socket  static   -      
systemd-journald-varlink@.socket static   -      
systemd-journald.socket          static   -      
systemd-journald@.socket         static   -      
systemd-networkd.socket          disabled enabled
systemd-rfkill.socket            static   -      
systemd-udevd-control.socket     static   -      
systemd-udevd-kernel.socket      static   -      

14 unit files listed.
root@dminimal:~# systemctl list-unit-files --type slice
UNIT FILE                          STATE  PRESET
machine.slice                      static -     
system-systemd\x2dcryptsetup.slice static -     
user.slice                         static -     

3 unit files listed.
root@dminimal:~# systemctl list-unit-files --type mount
UNIT FILE                     STATE     PRESET  
-.mount                       generated -       
dev-hugepages.mount           static    -       
dev-mqueue.mount              static    -       
media-cdrom0.mount            generated -       
proc-sys-fs-binfmt_misc.mount disabled  disabled
sys-fs-fuse-connections.mount static    -       
sys-kernel-config.mount       static    -       
sys-kernel-debug.mount        static    -       
sys-kernel-tracing.mount      static    -       

9 unit files listed.

```  

systemctl list-units # Tot els units que estan arrencats en l'arrancada del sistema

systemd.unit=rescue.target # opcion del kernel que tenemos que ponder en el grub

```bash
systemctl isolate rescue.target # carrega el target rescue

systemctl isolate poweroff.target # carrega el target poweroff y s'apaga 
```
en el final de la linia linux de grub , tenemos que poner: init=/bin/bash

RUNLEVELS(init)
0 Poweroff.target
1 rescue.target
234 multi-user.target
5 graphical.target
6 reboot.target

```bash
systemctl set-default rescue.target
removed = /etc/systemd/system/default.target
create symlink /etc/systemd/system/default.target -> /lib/systemd/system/rescue.target
```
/lib/systemd/system -----> directori amb totes les units instal·lades 


rm /etc/systemd/system/default.target
ln -s /lib/systemd/system/multi-user.target /etc/systemd/system/default.target  


```bash
apt-get update
apt-get install -y apache2
dpkg -L apache2 | grep bin # Per trobar executables 
dpkg -L apache2 | grep service # Per trobar els seus services

```


# Systemctl comandes
```bash
# systemct _______ nom.service
# status, show(mostra tota la configuració del servei)
# start restart,stop ,reload
# enable, disable , is-active, is-enabled, mask(serveix per no cambiar la configuració d'aquest servei), unmask
 
```
Cuan un servei esta en enable es que: que encienda el servicio por defecto cuando se inicie el ordenador. 


/etc/systemd/system/multi-user.target.wants # todo lo que haya dentro se iniciará con el multi-user.target

afegir un demoni nou systemctl daemon-reload
```bash
cp /lib/systemd/system/apache2.service /lib/systemd/system/patata.service
systemctl daemon-reload 

```
