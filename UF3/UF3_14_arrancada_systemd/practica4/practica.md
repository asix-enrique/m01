# Extracció dels exercicis del Howto:
### Targets d’arrencada: 

- 1. En tots els casos verificar el target actual. Llisteu els processos i els serveis.
```bash
root@dminimal:~# systemctl get-default
multi-user.target

root@dminimal:~# ps aux
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root           1  0.2  0.3 167676 12176 ?        Ss   12:24   0:01 /sbin/init
root           2  0.0  0.0      0     0 ?        S    12:24   0:00 [kthreadd]
root           3  0.0  0.0      0     0 ?        I<   12:24   0:00 [rcu_gp]
root           4  0.0  0.0      0     0 ?        I<   12:24   0:00 [rcu_par_gp]
root           5  0.0  0.0      0     0 ?        I<   12:24   0:00 [slub_flushwq]
root           6  0.0  0.0      0     0 ?        I<   12:24   0:00 [netns]
root           9  0.0  0.0      0     0 ?        I    12:24   0:00 [kworker/u4:0]
root          10  0.0  0.0      0     0 ?        I<   12:24   0:00 [mm_percpu_wq]
root          11  0.0  0.0      0     0 ?        I    12:24   0:00 [rcu_tasks_kt]
root          12  0.0  0.0      0     0 ?        I    12:24   0:00 [rcu_tasks_ru]
root          13  0.0  0.0      0     0 ?        I    12:24   0:00 [rcu_tasks_tr]

root@dminimal:~# systemctl list-unit-files --type service
UNIT FILE                              STATE           PRESET  
apache-htcacheclean.service            disabled        enabled 
apache-htcacheclean@.service           disabled        enabled 
apache2.service                        disabled        enabled 
apache2@.service                       disabled        enabled 
apparmor.service                       enabled         enabled 
apt-daily-upgrade.service              static          -       
apt-daily.service                      static          -       
autovt@.service                        alias           -       
console-getty.service                  disabled        disabled
console-setup.service                  enabled         enabled 

```
- 2. Configurar el sistema establint default.target a mode multi-user.target.
```bash
# Una manera podría ser així:
root@dminimal:~ rm /lib/systemd/system/default.target
root@dminimal:~ ln -s /lib/systemd/system/multi-user.target /lib/systemd/system/default.target
root@dminimal:~ systemctl get-default
    multi-user.target
# Un altra manera :
root@dminimal:~ systemctl set-default multi-user.target
    Removed "/etc/systemd/system/default.target".
    Created symlink /etc/systemd/system/default.target -> /lib/systemd/system/multi-user.target.
Aroot@dminimal:~ systemget-defaultget 
multi-user.target




```
- 3. Manualment canviar a graphical.target amb isolate.
```bash
root@dminimal:~ systemctl isolate graphical.target
# Després hauriem d'iniciar sesió :) 

```
- 4. Reiniciar el sistema i al grub etablir l’opció de iniciar en mode emergency.target.
  
### Demostració:  (al final de la linia linux posarem systemd.unit=emergency.target)
![imagenGrubRescue](img/grub.png)

- 5. Reiniciar i indicar al grub l’opció del kernel per iniciar a rescue.target.  
### Posarem un 1 que es mode rescue.target, al final de la linea linux  , i no cal posar systemd.unit=rescue.target
![imagenGrubRescue](img/rescue.png)

- 6. Canviar de target amb l’ordre isolate activant multi-user.target.
```bash
systemctl isolate multi-user.target
```
- 7. Restablit per defecte graphical.target i reiniciar el sistema.
- 8. Amb isolate indicar que es vol accedir al target poweroff.target.
- 9. Iniciar el sistema en mode emergency.target. Llistar els processos, l’arbre de
processos, els units i les dependencies. Cal indicar el password de root? Hi ha
multiples sessions de consola?
- 10. Iniciar el sistema en mode init=/bin/bash. Llistar els processos, l’arbre de processos,
els units i les dependencies. Cal indicar el password de root? Hi ha multiples
sessions de consola?

