# Introducion

- adm  disc
```bash
du          
df -h  
fdisk   
lsblk  
blkid  
tree /dev/disk  
mkfs  
fsck   
mount
si podemos haremos al final de todo esto:
resize  
    - grow   
    - shrink  
gparted
```

## Discos 
- discos rotacionales 
- SSD   
    (HD,USB...)
- HD (hard disk)

- Lo primero que haremos a un disco es hacer particiones  
- El disco tiene una estructura , que antes de las particiones ,   
  hay un espacio, MBR/BOOT  
- Para hacer particiones hay dos tecnologias  
   (MBR/DOS)  
       O  
      GPT(la mas nueva)  
GPT = GUID Partition Table  
GPT nos permite hacer particiones hasta 128(maximo)  

MBR solo nos permite hacer 4 particiones primarias
   1 de ellas puede ser extendida
   y la extendida puede tener dentro particiones lógicas
   (14 particiones ejemplo)

## Sistema de nombres de disco en LINUX 
nombres = primero (/dev/sda) y a continuacion el numero de partición:
- Disco /dev/sda
    primera particion /dev/sda1
    segunda particion /dev/sda2
- Disco 2 /dev/sdb
    primera particion /dev/sdb1
    segunda particion /dev/sdb2

### Particiones en MBR
/dev/sda1
/dev/sda2
/dev/sda3
/dev/sda4(extendida)
/dev/sda5(lógica)
 
- no es no mismo extendida que lógica , extendida es lo que hay fuera de las particiones lógicas .
- las lógicas siempre empiezan apartir de la 5

HD
/dev/sdb1(extenida)
/dev/sdb5 Debian (lógica)
/dev/sdb6 Fedora (lógica)
/dev/sdb7 Swap  (lógica)  

 
### Maquina debian
```bash
#apt-get install udpate
#apt-get install openssh-server openssh-client
#systemctl start ssh
#systemctl status ssh 
```
esto lo hacemos para poder hacerlo por ssh 


Disc físico   
esta dividido en pistas  
y sectores
1 sector son 512 bytes

agrupaciones de pistes es un (cluester)
Esto nos indica que tipo de esquema tiene o si MBR o GPT
en este caso es MBR:  
Disklabel type: dos

### comando `dd` crear discos
dd if=/dev/zero of=disk01.img bs=1k count=4M  status=progress    
sudo dd if /dev/zero of=disk01.img bs=1k count=1M status=progres  

`un /dev/zero es un disco que vomita 0 `
if = input file   
of = output file  
bs = block size
count = cuenta todas las veces que quieres que haga bs , en este ejemplo hacemos 4 millones de veces 1k que son 4GB  
- Ejemplo de lo que hace:
```bash
dd if=/dev/zero of=disk01.img bs=1k count=4M status=progress
4135678976 bytes (4.1 GB, 3.9 GiB) copied, 11 s, 376 MB/s
4194304+0 records in
4194304+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 11.4397 s, 375 MB/s

root@m05:~# ls -lh disk01.img 
-rw-r--r-- 1 root root 4.0G Apr  3 11:51 disk01.img
root@m05:~# file disk01.img 
disk01.img: data

hexdump -C disk01.img 
00000000  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
```

- Ahora con el disco creado vamos a crear un esquema de particiones con fdisk 
```bash
Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (1-4, default 1): 
First sector (2048-8388607, default 2048): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-8388607, default 8388607): +2G
```
```bash
Command (m for help): n
Partition type
   p   primary (1 primary, 0 extended, 3 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (2-4, default 2): 
First sector (4196352-8388607, default 4196352): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (4196352-8388607, default 8388607): 

Created a new partition 2 of type 'Linux' and of size 2 GiB.
```
- para guardar todos los cambios vamos a tener que poner la opcion w
```bash
Command (m for help): w
The partition table has been altered.
Syncing disks.
```
- para ver que particiones tenemos 
```bash
Command (m for help): p
```
- para eliminar una partición
```bash
Command (m for help): d
Partition number (1,2, default 2): 2

Partition 2 has been deleted.
```
- para crear particiones extenidas
```bash 
Command (m for help): n
Partition type
   p   primary (1 primary, 0 extended, 3 free)
   e   extended (container for logical partitions)
Select (default p): e
Partition number (2-4, default 2): 2
First sector (4196352-8388607, default 4196352): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (4196352-8388607, default 8388607): 

Created a new partition 2 of type 'Extended' and of size 2 GiB.
```
- creamos particiones dentro de la particion lógica
```bash
Command (m for help): n
All space for primary partitions is in use.
Adding logical partition 5
First sector (4198400-8388607, default 4198400): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (4198400-8388607, default 8388607): +500M

Created a new partition 5 of type 'Linux' and of size 500 MiB.
```
- cambiar tipo de particion de Linux a Swap
```bash
Hex code or alias (type L to list all): 82

Changed type of partition 'Linux' to 'Linux swap / Solaris'.
```
### siempre que hacemos estos cambios podemos comprobarlos con la opción p 


## Particionado del disco

- primer paso , hacer las particiones.
- segundo paso assignar un file system (mkfs -t fstype device)  
- finalmente , en linux solo existe una unica raiz de sistema de ficheros , que integra todo en un disco duro. 
- tercer paso , mount -t typefs   
                origen deviceorigen mountpoint   
- Actualmente el directorio /tmp no existe en linux , es un ramdisk. 

![Esquema raíz](../img/raiz.png)

## Sistema de ficheros
msdos  
vfat (evolucion de msdos)  
ntfs (windows)  
ext4 (ext,ext2,ext3...) ext4 tiene un sistema de journaling , ficheros transsacionales  
reiserfs  
xfs    
swap (no es un sistema , pero también como formato de partición. )


## Comandos mount
```bash
sudo locate label | grep bin

mount -t ext4 /dev/sda1 /dades

mount -t ntfs /dev/sda2 /tmp/win
mount -t ext4 /dev/sda3 /tmp/linux
umount /dev/sda3
sudo mount -t ext4 /dev/sda3 /mnt
df -h
df -h -t ext4 
df -h -t tmpfs


- mount a secas muestra todo lo que esta montado.    
mount -t ext4 podemos ver todos los sistemas de fichero ext4    
sudo mkfs.ext4 /dev/sda1 
sudo mount -t ext4 /dev/sda1 /tmp/dades  

haciendo la comanda mount -t tmpfs 
nos muestra todos los sistema de ficheros creados temporalmente
- umount
sudo umount potserDisc O desti
sudo umount /dev/sda2
sudo umount /tmp/linux

```

### dd haciendo imagenes

```bash
### Apartir de sda2 hacemos una imagen
sudo dd if=/dev/sda2 of=/dades/windows.img status=progres

### Planchar particion 3 en particion 2 
sudo dd if=/dev/sda3 of=/dev/sda2

### Planchar imagen windows a sda3
sudo dd if=/dades/windows.img of=/dev/sda3
sudo mount -t ntfs /dev/sda3 /tmp/win

## /dev/zero
sudo dd if=/dev/zero of=/tmp/dades/swap.img bs=1k count=500k status=progres # = disco de 500MB
```  
```bash
### Crear imagenes raw y convertir a fstype: ext4 , vfat,xfs , swap:
sudo dd if=/dev/zero of=/dades/nombre.img bs=1k count=500k # == disco de 500MB
``` 


## swap 
mkswap /dev/sda4
sudo swapon /dev/sda4 (montar partición swap, si no, no saldrá )  
sudo swapoff /dev/sda4 (desmontar partición swap , para que no salga.) 

- creamos un disco falso y lo hacemos apra que sea el mkswap:
```bash
sudo dd if=/dev/zero of=/tmp/dades/swap.img bs=1k count=500k # disco de 500MB
sudo mkswap /tmp/dades/swap.img # (aixo per formatejar que /tmp/dades/swap.img sea swap)
sudo swapon /tmp/dades/swap.img # (aixo per muntar la partició swap)
sudo mkswap /dev/sda3
sudo swapon /dev/sda3 # (Amb aixo tenim 2 particions de swap)
sudo swapoff -a #  (Desmontamos todos los swap)
sudo swapon -a # Lee todas las lineas del fichero fstab para saber que tiene que montar. 
```
poner etiqueta a swap es 
sudo swaplabel /dev/sda3 etiqueta

## fstab
- file system tabulation
Es un fichero que a cada linea , dice que tiene que montar a cada linea.  

## montar por etiqueta
sudo ntfslabel /dev/sda2 windows # crear etiqueta
sudo mount -t ntfs -o ro LABEL=windows /mnt

## montar por UUID
sudo mount -t ext4 -o rw UUID="109EF1744BEE0468" /mnt

## cambiar permisos mount de rw a ro
sudo mount -t ext4 -o remount,ro /mnt 
ahora si queremos introducir algo en mnt no dejará y pondrá que es read only


## Formato xfs
sudo mkfs.xfs -f /dev/sda3

## formato reiser
```bash
sudo mkfs.reiserfs -f /dades/swap.img 
sudo mount -t reiserfs -o loop /dades/swap.img /mnt
sudo losetup -a
sudo losetup -f # Preguntar que loop esta free
sudo losetup /dev/loop1 disk01.img # asociar
sudo losetup -d /dev/loop1 # desasociar loop 
sudo mkfs.ext4 /dev/loop1  
```
## losetup 
-a llista  
-f primer disponible  / primer free
losetup /dev/loop_ file asociar
-d desasociar

## loop

- /dev/loop{nº} = particions virtuals  

## device 
`device, mountpoint , fstype, dump, check`  
file.img  
puede ser un recuros remoto
esto son unidades de red

options: 
    `rw` , read write
    `ro` , read only
    `suid` , que se pueda usar suid , nosuid que no se pueda usar suid (esto suele estar habilitado  
    para no tener software malicioso.)  
    `exec` , ejecutables
    `auto/noauto` , que se monte automaticamente o no , las que pongan no auto , que las definimos ,  
    pero no las montamos.  En los dispositivos externos , son noauto.  
    `async` , aporta seguridad para poder guardar datos .  

## fstab: 
LABEL=linux /tmp/linux ext4 defaults 0 0   
UUID=1091744BEEADA /tmp/win ntfs ro,defaults 0 0 # con ro, y defaults son todas pero solo lectura , no escritura. 
/dev/sda3 swap swap defaults 0 0 
tmpfs /mnt/temporal tmpfs size=100M,defaults 0 0 
## amb fdisk hacer particiones tema MBR y GPT   
## cambiar con mount permisos
sudo mount -t ext4 -o remount,ro /mnt/linux
## resize:
shrink empetitir   
grow creixer  
ejemplos:
fs disminuir
borrar la partición   
crear de nuevo la partición  
el primer sector tiene que ser el mismo. 
no borrar la signatura
partición tiene que ser grande para contener fs

Ejercicios:  
sudo umount /dev/sda3  
sudo e2fsck -f /dev/sda3  
sudo resize2fs /dev/sda3 900M  
comprobar la particion creada:  
sudo mount /dev/sda3 /mnt/linux 
df -h 
 
ahora tendriamos que hacer varios pasos con el fdisk  

## LABELS
```bash 
sudo e2label /dev/sda1 dades  
sudo ntfslabel /dev/sda2 windows   
sudo e2label /dev/sda3 linux  
sudo swaplabel /dev/sda4 swap
# ver todas las label  
sudo blkid 
```

CD DVD / .iso   
        .iso9660 
# Docker:
docker run --rm -v $(pwd):/prova --privileged -h m01 -it debian
```bash
sudo apt-get install -y tree vim git mkisofs
cd /prova
sudo git clone https://gitlab.com/edtasixm01/M01-operatius.git
sudo mkisofs -o m01.iso M01-operatius
```

## Crear disco virtual  
sudo mkdir /mnt/discvirt  
sudo mount -t tmpfs -o size=100M tmpfs /mnt/discvirt  

## comandos clase
sudo df -h -t ext4 -t fuseblk # esto lo que hace es buscar los tipos que hay de ext4 y fuseblk  




# 104.2 
fsck  
e2fsck [-f] 
tune2fs [-l (listar tota la informació de la partició)]  
        [-i (cada cuantos días)]  
        [-c (cada cuando se monte)] 
dumpe2fs  
```bash
df
    -h
    -t
```
## particion que se haga un check cada 180 dias o 3 veces.
sudo tune2fs -c3 -i180 /dev/sda1


sudo tune2fs -c1 -i2 /dev/sda3 # cada vez que se monta o cada 2 días

## como saber todos los super blocks 
con dumpe2fs y grep superblocks

sudo dumpe2fs /dev/sda3 | grep superblock


## Shrink and grow  
shrink 
-1 hacer pequeño sistema de ficheros
  e2fsck  
  resize2fs  
-2 hacer pequeña la particion  
  -a borrar la particion(no borrar la señal)
  -b crear la particion de nuevo(
    el primer sector tiene que ser el mismo)  
  -c que la particion sea grande para que quepa el sistema de ficheros

-3 volver hacer el file sistem tan grande como la particion  
  resize2fs   

grow  
-1 hacer crecer la partición 
  borrar y crear de nuevo la partición , respectando el primer sector  
  respectando el no borrar la señal   
-2 incrementar file system  
  e2fsck
  resize2fs



shink ejercicio:
esto en root  
e2fsck -f /dev/sda1  
resize2fs /dev/sda1 2900M  
fdisk /dev/sda  
dentro hacemos  
d, 1 , n ,p , 1 , enter , +3G   
