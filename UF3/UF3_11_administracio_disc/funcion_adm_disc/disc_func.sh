#!/bin/bash
#
# @edt ASIX M01
#
# Autor: Enrique Lorente
#
# Exemples funcions de disc
#------------------------------

function fsize(){
	# Validate which user exists
	login=$1
	line=$(grep "^$login:" /etc/passwd)
	if [ -z "$line" ]; then
		echo "Error..."
		echo "$login not exist"
		return 1 
	fi
	# Search home	
	dirHome=$(echo $line | cut -d: -f6)
	du -sh $dirHome	
}


# Function rep arguments que són logins 
# i calcula el fsize del home
function loginargs(){
	# Validate num args 
	if [ $# -eq 0 ]; then 
		echo "Num arg incorrecte!!"
		echo "Usage: function logins"
		return 1
	fi 
	for login in $* 
	do 
		fsize $login	
	done 
}

# Function rep un argument que és un fitxer
# cada línia del fitxer té un login 
function loginfile(){
	if [ ! -f $1 ]; then
		echo "Error... $1 not exist"
		return 1
	fi 
	file=$1
	while read -r login 
	do
		fsize $login
	done < $file
}

# Rep logins per la entrada standard
# per cada un d'ells calcula fsize
function loginstdin (){
	while read -r login 
	do
		fsize $login
	done 

}

# Processa el fitxer rebut o 
# stdin si no en rep cap
function loginboth(){
	fileIn="/dev/stdin" 
	if [ $# -eq 1 ]; then 
		fileIn=$1
	fi
	while read -r login
	do
		fsize $login
	done < $fileIn 
}

