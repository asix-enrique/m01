#!/bin/bash
#
# @edt ASIX Enrique Lorente
#
# Funció que rep nom d'usuari i mostra cuant espai té el seu home
#----------------------
#

# funcion 

function space_home_by_user_id(){
	if [ "$#" -ne 1 ]; then
        	echo "Error! num arg incorrecte"
		echo "Usage: $0 <user_id>"
        	return 1
    	fi
	user_id=$1
	
	username=$(grep "^$user_id:" /etc/passwd )
	if [ -z "$username" ]; then
		echo "User with ID $user_id does not exist."
		return 2
	fi 

	home_dir=$(echo $username | cut -d: -f6)

	space_total=$(du -s "$home_dir" | cut -f1)

	echo $space_total
}
