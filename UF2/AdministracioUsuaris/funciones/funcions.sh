#!/bin/bash
#
# Autor: Enrique
# Març 2024
# Funcions 
#
#
#-----------------------------

function suma(){
	echo $(($1+$2))
	return 0 
}

function showUserByLogin(){
# 1) Validar Num args
	if [ $# -eq 0 ]; then
		echo "Error! Num args incorrecte"
		echo "Usage: showUserByLogin login"
		return 1
	fi
	login=$1
	grep -q "^$login:" /etc/passwd
	if [ $? -ne 0 ]; then
		echo "Error! login $login no existe" 
		return 2 
	fi 

	grep "^$login:" /etc/passwd | sed -r 's/^([^:]*):[^:]*:([^:]*):([^:]*):[^:]*:[^:]*:([^:]*).*$/login: \1 \nuid: \2 \ngid: \3 \nshell: \4/' 
	return 0 
}

showUserClase(){
	login=$1
	line=$(grep "^$login:" /etc/passwd)
	if [ -z "$line" ]; then
		echo "Error! no existe el login: $login"
		return 1
	fi
	uid=$( echo $line | cut -d':' -f3 )
	gid=$( echo $line | cut -d':' -f4 )
	shell=$( echo $line | cut -d':' -f7)
	echo "login: $login"
	echo "uid: $uid"
	echo "gid: $gid"
	echo "shell: $shell"

}

function showUsersInGroupByGid(){
	# 1) validar args
	# 2) validar existeix grup 
	# 	mostrar gname(gid)
	# login , uid, shell , dels usuaris que pertañen al gid indicat.
	giduser=$1
	linegid=$(grep "^[^*]*:[^*]*:$giduser:" /etc/group )
	if [ -z "$linegid" ]; then
		echo "Error! no existe el gid: $gid"
		return 1 
	fi
	echo "gname($giduser)" | cut -d':' -f1
	line=$(grep "^[^:]*:[^:]*:[^:]*:$giduser:" /etc/passwd)
	for user in $line
	do
		echo "$user" | cut -d: -f1,3,7
	done
	return 0 
}

function showAllShells(){
	MIN=$1
	if [ $# -eq 0 ]; then
		echo "Error! num args incorrecte"
		echo "Usage: showAllShells minimusers"
		return 1
	fi
	# lista de todos los shells
	llistaShells=$(cut -d':' -f7 /etc/passwd | sort  | uniq)
	for shell in $llistaShells
	do
		# contar cuantos usuarios hay en cada shell 
		cuantsShells=$(grep ":$shell$" /etc/passwd | wc -l )
		if [ $cuantsShells -gt $MIN ]; then 
			echo -e "\nShell: ($cuantsShells) $shell\n"
			#Muestra los usuarios de cada shell tabulados
			grep ":$shell$" /etc/passwd | cut -d':' -f1,3,4 \
			 | sed -r 's/^(.*)$/\t \1/'	

		fi
	done
	return 0 
}


