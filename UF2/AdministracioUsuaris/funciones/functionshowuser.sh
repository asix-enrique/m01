#!/bin/bash
#
#
#
# rep un login: login, uid , gid , shell 
# ----------------------------

function showUser(){
	login=$1
	grep -q "^$login:" /etc/passwd
	if [ $? -ne 0 ]; then
		echo "Error! login $login no existe" 
		return 1 
	fi 

	grep "^$login:" /etc/passwd | sed -r 's/^([^:]*):[^:]*:([^:]*):([^:]*):[^:]*:[^:]*:([^:]*).*$/login: \1 \nuid: \2 \ngid: \3 \nshell: \4/' 
	return 0 
}

