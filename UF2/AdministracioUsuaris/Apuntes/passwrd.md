# Passwords

- llarges
- facils de recordar 
- han de ser en un punt politicament incorrecte 
- frase que es pugui memoritzar


## Password policy 

- /etc/login.defs
- chage

### man passwd 
man 5 passwd  
  
                                        warning  inactive  
canviat contraseña|----|--------------|--------|----------|-----------> 29-08 caduca el compte  
-------------------|MIN------------------------MAX---------------------> account expiration date  
-------------------|3DIES                     

comandos  
passwd -S pere (-S , status)  
pere P 2024-03-13 0 99999 7 -1  

passwd -l anna (-l, lock = bloquear contraseña, no deja entrar)  
root@host:/# passwd -S anna  
anna L 2024-03-13 0 99999 7 -1  

passwd -d anna passwd-less (-d , delete = sirve para borrar la contraseña para que no haga falta poner ninguna contraseña)    
passwd -e anna (-e , expire = una vez iniciemos como anna deberiamos cambiar la contraseña)    


/etc/login.defs  
el valor del gid esta repartit en login.defs     
va del 1000 al 60000  

USERGROUPS_ENAB yes = crea el grupo del ususario creado si no se asigna uno.   


## chage 
list caracteris,  -l   
chage -l anna  

-m = min days , -M = maximum days , -W warning , -I inactive  
chage -m 3 -M 30 -W 3  -I 2  anna   
chage -l anna  

chage -E 2024-06-01 anna expire el 2024-06-01 YYYY-MM-DD  

chage -E $(date -d +3days +%Y-%m-%d) pere   


## Date 

date -d +19days +%Y-%m-%d   
chage -E $(date -d +19days +%Y-%m-%d) pere  


