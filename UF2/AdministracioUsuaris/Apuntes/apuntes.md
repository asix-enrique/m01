-----1 Adm usuaris
crear / borrar/modificar
opcions
politica de passwords
scripts d'inici de sessió 



-----2
Tot usuari té 
	· UID
	· GID grup principal
	· pot tenir més grups secondaris

-----3 Fitxers usuaris/groups
usuaris {  
	/etc/passwd  
	/etc/shadow	  	
}  

groups {  
	/etc/group  
	/etc/gshadow**"aquest no el tocarem molt"  
}  

En /etc/passwd, esta guardada la información del grupo principal que pertenece el usuario 
y para mirar los grupos secundarios tenemos que ir a /etc/group


#Comandos para creacion de container  
docker run --rm --name host -h host -it debian  
apt-get update  
apt-get install -y vim less tree passwd  

# Els passwords dels usuaris estan amb hash
user01:$y$j9T$ke8NuoEyukdpvaYDZZt1A.$8LIwJDae6y..P/FjjBdoCmzVsXwVn2UJ/wVLThgdfSD:19786:0:99999:7:::  

# Passwd
login
passwd
UID
GID
GECOS
dir-home
shell

# Creació usuari amb Gid = 100 UID = 1100 i la seva shell /bin/bash
useradd -u 1100 -g 100 -s /bin/bash user02

### user01 crar home , /bin/bash grups secondaris = users,mail
useradd -m -s /bin/bash -G 100,8 user01
useradd -m -s /bin/bash -G users,mail user01

#### user02 no es creii un grup propi crear home 
root@host:/# useradd -N -m user02   
root@host:/# id user02  
uid=1002(user02) gid=100(users) groups=100(users)  

#### alum01 /bin/bash , creei home /home/alumnes
useradd -m -b /home/alumnes -s /bin/bash alum01  

#### pro01 /bin/bash , creei , home personal sea este /home/inf/proj/compartit

useradd -d /home/inf/proj/compartit -s /bin/bash pro01  

#### borrar usuaris 
userdel -r usuari = borrar usuario con el home etc...
userdel usuari

no es veritat queel home del usuari estigui dins de /home/nomi  

## useradd 
-N no crear grup propi    
-m crear directori personal    
-s /bin/bash [shell]    
-u UID    
-g GID/gname    
-G gids/gnames | -aG (-a append mes groups )    
-b base directori [/home/users]/marta dins de [/home/users/] se crearan els directoris      
-d dir-absolut --> /home/[dirabsoulut] aquest sería el directori d'en Ramon     
-D nomes serveix per configurar la conifugració default    

# Fichero configuracion useradd  
/etc/default/useradd  
## modificar default useradd  
root@host:/# useradd -D -g inf -s /bin/bash  
root@host:/# useradd -D  
GROUP=1000  
HOME=/home  
INACTIVE=-1  
EXPIRE=  
SHELL=/bin/bash  
SKEL=/etc/skel  
CREATE_MAIL_SPOOL=no  
LOG_INIT=yes  

## Cambiar shell   
chsh -s /bin/bash pere  


ls -la /etc/skel/    
es el directori esquelet del directori per defecte dels usuaris.    
useradd -m -k /etc/skel_alumnes pere    
 cd /tmp/projectes/pere    
root@host:/tmp/projectes/pere# ls -la    
total 20    
drwxr-xr-x 2 pere pere 4096 Mar 11 08:20 .    
drwxr-xr-x 3 root root 4096 Mar 11 08:20 ..     
-rw-r--r-- 1 pere pere  220 Apr 23  2023 .bash_logout    
-rw-r--r-- 1 pere pere 3526 Apr 23  2023 .bashrc    
-rw-r--r-- 1 pere pere  807 Apr 23  2023 .profile    
-rw-r--r-- 1 pere pere    0 Mar 11 08:18 alumnes    
-rw-r--r-- 1 pere pere    0 Mar 11 08:18 examens    
-rw-r--r-- 1 pere pere    0 Mar 11 08:18 normes    
useradd -N -G basket,fumbol -m pere     

groupdel pere     
no es pot borrar un group que el group es el group primary de aquest usuari.    
cuan s'esborra un usuari i te un group propi també es borra per exemple usuari pere amb group pere , es borra tots dos.      
root@host:~# useradd -m -G users,basket pere    
root@host:~# groupdel pere    
groupdel: cannot remove the primary group of user 'pere'    
root@host:~# usermod -g basket pere      
root@host:~# groupdel pere    
root@host:~# id pere    
uid=1000(pere) gid=1001(basket) groups=1001(basket),100(users)  
root@host:~#        


## skel 

skel default
.bash_logout
.bashrc
.profile



## Mirar commands and files
- . 
- source 
- /etc/bash.bashrc
- /etc/profile
- /etc/bash_logout
- env
- export 
- set 
- unset
- alias
- function(no cal)
- login.defs
- finger
- chage

## Entorn del usuari , en la sessió actual
- Variables  
- alias  
- function  
- startup programs  
todo esto lo podemos mostrar lo del usuario actual con el orden `set`


- /etc/profile
	```
	Dentro de aqui tenemos variables y startup programs 
	```

- /etc/bash.bashrc

	```
	Dentro de aqui tenemos funciones y alias  
	```
Basicamente esto son ficheros de personalización del sistema (només root)     
enviroment = tota la configuració predefinida   
amb export podemos llevar las variables creadas como `nom="pepito"` pero cuando cerremos la session todo se borrará.  
`export nom edat`  

Tambien cada usuario tiene sus ficheros:    
.bashrc    
.profile     
.bash_logout    
estos ficheros vienen establecidos en el /etc/skel  

`.bashrc` {  
	alias y funciones  
}  
`.profile` {  
	variables y startup programs  
}  
`.bash_logout` {  
	acciones al cerrar la session  
}  


 