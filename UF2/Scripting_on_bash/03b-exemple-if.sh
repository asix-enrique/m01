#!/bin/bash
# @edt ASIX M01
#
# Exemples ordre if
# ---------------------------------------------------------
#
# 1) Validar Arguments
if [ $# -ne 1 ]
then
  echo "Error: numero arguments incorrecte"
  echo "Usage: $0 edat" 
  exit 1
fi

# 2) xixa
edat=$1
if [ $edat -ge 18 ]
then 
  echo "edat $edat és major d'edat"
else 
  echo "edat $edat és menor d'edat"
fi
exit 0

