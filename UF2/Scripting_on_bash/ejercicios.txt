12-for-notes.sh
Programa que rep almenys una nota o més  i per cada nota diu si és un suspès, aprovat, notable o excel·lent.
a) el programa valida que el número d'arguments és correcte.
b) per cada nota valida que el valor és entre 0 i 10. Si no és correcte es mostra un missatge per stderr però es continua iterant la resta de notes.
c) per cada nota vàlida mostra quina qüalificació li correspòn.

12-for-mesos.sh
Programa que rep almenys un més  o varis mesos. Per cada més indica quants dies té el més. 
a) el programa valida que el número d'arguments rebuts és vàlid.
b) el programa valida per cada més que el seu valor és del 1 al 12. Si no ho és es mostra un missatge d'error per stderr però es continua processant la resta de mesos.
c) per cada més s'indica quants dies té.

