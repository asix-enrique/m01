#!/bin/bash
# Febrer 2024
# @edt ASIX M01
# while entrada standard ejercicios
# ------------------------

# 9 ) procesar linia a linia un file a majuscules
fileIn=$1
comptador=1
while read -r line
do
  chars=$(echo $line | wc -c)
  echo "$comptador: ($chars) $line" | tr 'a-z' 'A-Z'
  ((comptador++))
done < $fileIn
exit 0 


# 8) procesar linia a linia un file
fileIn=$1
comptador=1
while read -r line
do
  echo "$comptador: $line"
  ((comptador++))
done < $fileIn
exit 0 

# 7) numerar i mostrar en majuscule stdin
num=1
while read -r line
do
  echo $num: $line | tr 'a-z' 'A-Z'
  ((num++))
done 
exit 0 
#6) itera linea a linea fins a token (per exemple FI)
TOKEN="FI"
num=1
read -r line
while [ "$line" != $TOKEN ]
do
  echo "$num: $line"
  read -r line
  ((num++))
done
exit 0  
# 5) numerar les linies rebudes
comptador=1
while read -r line
do
  echo "$comptador: $line"
  ((comptador++))
done
exit 0 
# 4) processar stdin linia a linia

while read -r line
do
  echo $line 
done
exit 0 
