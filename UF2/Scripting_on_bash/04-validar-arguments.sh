#!/bin/bash
# @edt ASIX-M01 Curs 2023-2024
# Validar que té exàctament 2 args
# i mostrar-los
#
# 1)Validar que hi ha 2 args
#

if [ $# -ne 2 ]
then
  echo "Error: número de arguments incorrecte"
  echo "Usage: $0 nom cognom"
  exit 1
fi

# 2) Xixa
#
echo "nom:$1 
cognom:$2"

