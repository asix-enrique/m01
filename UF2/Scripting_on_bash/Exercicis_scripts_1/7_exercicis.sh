#!/bin/bash
# Febrer 19-02-2024
#
# @edt ASIX M01 Enrique Lorente
# 
# Exercici 7
#
# Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la
# mostra, si no no.
#-------------------------------------------------

while read -r line; do
	if [ $(echo "$line" | wc -c) -gt 60 ] 
	then
	    echo $line
	fi 
done
exit 0

