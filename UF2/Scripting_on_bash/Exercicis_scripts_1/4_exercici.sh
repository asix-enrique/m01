#!/bin/bash
# Febrer 19-02-2024
#
# @edt ASIX M01
# 
# Exercici 4
#
# Fer un programa que rep com a arguments números de més (un o més) i indica per
# a cada mes rebut quants dies té el més.
#--------------------------------------------------

# 1) Validar num arguments
ERR_ARG=1
if [ $# -lt  1 ] 
then 
  echo "Error: numero args incorrecte"
  echo "Usage: $0 posar num mes 1 o més " 
  exit $ERR_ARG 
fi

# 2) Validar per cada mes que el seu valor és del 1 al 12. 
# Si no ho és mostra un missatge d'error per stderr per continua processant la resta de mesos.

for mesos in $@
do
	if ! [ $mesos -ge 1 -a $mesos -le 12 ]
	then
  		echo "Error: El numero de mes $mes es incorrecte, ha de ser -->[1-12]" >> /dev/stderr
	else
 	case $mesos in
 		1|3|5|7|9|10|12)
        		 echo "El mes: $mesos te 31 díes"
		    	    ;;
	    	2)
			    echo "El mes: $mesos te 28-29 díes" 
			    ;;
	   	 *)
			    echo "El mes: $mesos te 30 díes"
			    ;;
		esac
	fi   
done 
exit 0
