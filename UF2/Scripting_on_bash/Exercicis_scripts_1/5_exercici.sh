#!/bin/bash
# Febrer 19-02-2024
#
# @edt ASIX M01 Enrique Lorente
# 
# Exercici  5
#
# Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.
#------------------------------------------------------


# 1) Mostrar línia a línia l'entrada estàndard retallant els primers 50 caràcters

while read -r line	
do
  echo $line | cut -c-50
done
exit 0
