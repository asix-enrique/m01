#!/bin/bash
# Febrer 19-02-2024
#
# @edt ASIX M01 Enrique Lorente
# 
# Exercici 6
#
# Fes un programa que rep com argument nos de dies de la setmana i mostra quants dies eren laborables i
# quants festius
# Si l'argument no és un dia de la setmana genera un error per stderr
#--------------------------------------------------
#

# 1) Validar num args

ERR_ARG=1
if [ $# -lt 1 ]
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 posar 1 o mes dies de la setmana "
  exit $ERR_ARG
fi

# 2) Validar el nom del dies de la setmana i cuants díes son laborables o festius
comptador_dia_lab=0
comptador_dia_fest=0
for diasetmana in $* 
do
	case $diasetmana in 
		dilluns|dimarts|dimecres|dijous|divendres)
			((comptador_dia_lab++))
			;;
		dissabte|diumenge)
			((comptador_dia_fest++))
			;;
		*)
			echo "$diasetmana es incorrecte"
			;;
	esac		
done
echo "Dies laborables: $comptador_dia_lab"
echo "Dies festius: $comptador_dia_fest"
exit 0 

