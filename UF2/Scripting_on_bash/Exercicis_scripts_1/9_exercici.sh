#!/bin/bash
# Febrer 19-02-2024
#
# @edt ASIX M01
#
# Exercici 9
#
# Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el
#sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra
# per stderr.
#
# -----------------------------

# 1) Rebre noms d'usuari 

while read -r line ; do 
	grep -q "^$line" /etc/passwd 
	if [ $? -eq 0 ]; then
		echo "$line [+]"
	else
		echo "$line [-]" >> /dev/stderr
	fi	
done
exit 0 
