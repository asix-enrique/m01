#!/bin/bash
# Febrer 19-02-2024
#
# @edt ASIX M01
#
# Exercici 8
#
# Fer un programa que rep com a argument noms d’usuari, si existeixen en el sistema
# (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per
# stderr.
#------------------------------------------------

# 1) Validar num args
ERR_ARG=1

if [ $# -eq 0 ] ; then
  echo "Error! Num args incorrecte"
  echo "Usage: $0 nomsd'Usuari"
  exit $ERR_ARG
fi

# 2) Validar si l'usuari existeix al fitxer /etc/passwd 

usuari=$*

for usuaris in $usuari
do
	grep -q "^$usuaris" /etc/passwd 
	if [ $? -eq 0 ];then
	  echo "El usuari $usuaris si existeix en el fitxer /etc/passwd"
	else
	  echo "El usuari $usuaris no existeix al fitxer /etc/passwd" >> /dev/stderr 
	fi
done
exit 0
	 
 
