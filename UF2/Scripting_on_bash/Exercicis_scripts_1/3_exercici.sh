#!/bin/bash
# Febrer 19-02-2024
#
# @edt ASIX M01
# 
# Exercici 3
# Fer un comptador des de zero fins al valor indicat per l'argument rebut.
#-----------------------

# 1) Validar arguments

if [ $# -ne 1 ]; then
  echo "Error: num args incorrecte"
  echo "Usage: $0 número"
  exit 1
fi

# 2) fer comptador fins que arribi a l'argument rebut

comptador=0
MAX=$1
while [ $comptador -le $MAX ] 
do 
	echo "$comptador"
	((comptador++))
done
exit 0 
