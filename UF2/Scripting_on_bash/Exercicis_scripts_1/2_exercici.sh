#!/bin/bash
# Febrer 19-02-2024
#
# @edt ASIX M01
#
# Exercici 2
#
# Mostrar els argumetns rebuts línia a línia , tot numerànt-los
# ----------------
#

# 1) Validar número args
ERR_ARG=1
if [ $# -lt 1 ]; then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 posar 1 o més arguments " 
  exit $ERR_ARG 
fi
# 2) Mostrar els arguments numerats línia a línia
comptador=1
for args in $@
do
	echo "$comptador: $args"
    	((comptador++))
done
exit 0
