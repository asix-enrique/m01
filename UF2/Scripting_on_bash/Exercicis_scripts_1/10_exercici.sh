#!/bin/bash
# Febrer 19-02-2024
#
# @edt ASIX M01
#
# Exercici 10
#
#
#
#Fer un programa que rep com a argument un número indicatiu del número màxim de
#línies a mostrar. El programa processa stdin línia a línia i mostra numerades un
# màxim de num línies.
#---------------------------------------

# 1 ) Validar num args
if [ $# -ne 1 ]; then
  echo "Error: num args incorrecte"
  echo "Usage: $0 número"
  exit 1
fi


# 2) procesar línea a línea 
comptador=0
MAX=$1
while [ $comptador -lt $MAX ]; do
	read -r line
	echo "$line"
	((comptador++))
done
exit 0

