#!/bin/bash
# Febrer 19-02-2024
# 
# @edt ASIX M01
#
# Exercici 1
#
# Mostrar l'entrada estàndard numerant línia a línia 
# ------------------
#

# 1) Entrada estàndard línia a línia
comptador=1
while read -r line
do
  echo "$comptador: $line"
  ((comptador++))
done
exit 0 
