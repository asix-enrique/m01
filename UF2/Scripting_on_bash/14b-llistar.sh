#!/bin/bash
# Febrer 26-02-2024
# @edt ASIX M01 Enique Lorente
#
#
# 	a ) Rep un arg i és un directori i es llista
#	b ) Llistar numerant els elements del dir
# ------------------------
#

# 1) Validar num args
ERR_NARGS=1
if [ $# -ne 1 ]
then
  echo "Error: número de args incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# 2) Validar que és un dir
directori=$1
if  [ ! -d $directori ]; then
	echo "Error: $directori no és un directori"
	echo "Usage: $0 dir"
	exit 2 
fi

# 3) Llistar
comptador=1
llista=$(ls $directori)
for elem in $llista; do
	echo "$comptador: $elem"
	((comptador++))
done
exit 0 
