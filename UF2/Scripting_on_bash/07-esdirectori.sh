#!/bin/bash
# @ edt ASIX-M01 Curs 2023-2024
#
# llistar el directori rebut:
# 	a) verificar rep un arg
# 	b) verificar que és un directori
#----------------------------------------
ERR_NARGS=1
ERR_DIRECT=2
# 1)Validar arguments
if [ $# -ne 1 ]
then
  echo "Error! número de args incorrecte"
  echo "Usage: $1 directori"
  exit $ERR_NARGS
fi
# 2) Validar directori
if [ ! -d $1 ]
then
  echo "Error: $1 no es un directori"
  echo "Usage: $0 directori"
  exit $ERR_DIRECT
fi

ls $1
