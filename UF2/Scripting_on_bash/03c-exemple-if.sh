#!/bin/bash
# @edt ASIX M01
#
# Enrique Lorente
#
# Exemples ordre if
# ---------------------------------------------------------
#
# 1) Validar Arguments
if [ $# -ne 1 ]
then
  echo "Error: numero arguments incorrecte"
  echo "Usage: $0 edat" 
  exit 1
fi

# 2) xixa
edat=$1
if [ $edat -lt 18 ]
then 
  echo "edat $edat és menor d'edat"
elif [ $edat -le 65 ]
then  
  echo "edat $edat activa"
else 
  echo "edat $edat jubilada"
  exit 0
fi 
