#!/bin/bash
# Marzo 2024 
#
# Enrique Lorente
#
# Programa que procesa el contingut de un fitxer 
#  el fitxer i matricules per argument matricules 9999-AAA
#  OK(stdout)
#  err(stderr)
#  numeroerrors
#----------------------------------------------

status=0
# 1) Validar num args 
if [ $# -eq 0 ]; then
	echo "Error: Numero de arguments incorrecte"
	echo "Usage: $0 fitxer matricula1..... "
fi

#2) Primer arg es un fitxer

file=$1
#if ! [ -f $file ]; then
	
# fi

while read -r line; 
do
	echo $line | grep -E "^[A-Z,a-z]{4}[0-9]{3}$"
	if [ $? -ne 0 ]; then 
		echo "$line" >> /dev/stderr
		status=3
	fi
done < $file
exit $status

