#!/bin/bash
# Febrer 26-02-2024
#
# @edt ASIX M01 Enrique Lorente
#
# Exercici 4
#
#
# Processar stdin mostrant per stdout les línies numerades i en majúscules..
#------------------------------------------------------------

# 1)  Processar stdin 
comptador=0
while read -r line; do
	((comptador++))
       	echo "$comptador: $line" | tr '[a-z]' '[A-Z]'	
done
exit 0 
