#!/bin/bash
# Febrer 26-02-2024
#
# @edt ASIX M01 Enrique Lorente
#
# Exercici 3
#
#
# Processar arguments que són matricules: 
# 	a) Llistar les vàlides, del tipus: 9999-AAA. 
# 	b) stdout les que sòn valides, per stderr les no vàlides. Retorna de status 
# 	el número d'errors (de no vàlides)
#----------------------------

#1) Validar num args
ERR_ARG=1
if [ $# -lt  1 ]
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 arg1... "
  exit $ERR_ARG
fi

# 2) Llistar matricules valides o no 9999-AAA
llista_matricules=$*
NUM_ERR=0
for matricula in $llista_matricules; do 
	echo $matricula | grep -Eq "^[0-9]{4}-[A-Z]{3}$"
	if [ $? -eq 0 ]; then
		echo "$matricula"
	else
		((NUM_ERR++))
		echo "$matricula" >> /dev/stderr
	fi
	
done
exit $NUM_ERR
