#!/bin/bash
# Febrer 19-02-2024
#
# @edt ASIX M01 Enrique Lorente
#
# Exercici 1
#
#
# Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
#----------------------------

# 1) Validar num args
ERR_ARG=1
if [ $# -lt  1 ]
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 arg1... "
  exit $ERR_ARG
fi

# 2) mostrar per stdout només si tenen més de 4 caràcters

for args in $*; do
 	echo "$args" | grep -E "^.{4,}"	
done
exit 0
