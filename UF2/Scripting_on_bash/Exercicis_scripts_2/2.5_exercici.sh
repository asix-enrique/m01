#!/bin/bash
# Febrer 26-02-2024
#
# @edt ASIX M01 Enrique Lorente
#
# Exercici 5
#
#
# Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
#------------------------------------------------------------
#
#
# 1) Processar stdin 
while read -r line; do
	echo "$line"  | grep -E "^.{,50}$"
done
exit 1

