#!/bin/bash
# Febrer 26-02-2024
#
# @edt ASIX M01 Enrique Lorente
#
# Exercici 6
#
#
# Processar per stdin línies d’entrada
# tipus “Tom Snyder” i mostrar per stdout la línia 
# en format → T. Snyder.
#------------------------------------------------------------

while read -r line
do 
	echo "$line" | sed -r 's/^(.)[^ ]* /\1. /'  
done
exit 0 
