#!/bin/bash
# Febrer 19-02-2024
#
# @edt ASIX M01 Enrique Lorente
#
# Exercici 2
#
#
#Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
#----------------------------

# 1) Validar num args
ERR_ARG=1
if [ $# -lt  1 ]
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 arg1... "
  exit $ERR_ARG
fi


# 2) Comptar quantes n'hi ha de 3 o més caràcters
comptador=0
for args in $*; do
	echo "$args" | grep -Eq "^.{3,}"
	if [ $? -eq 0 ]; then
		((comptador++))
	fi
done
echo "$comptador"
exit 0

