#!/bin/bash
# @edt Febrer 2024
#
# exemples while
# ---------------------------
#





#1) mostrar numeros del 1 al 10

MAX=10
num=1

while [ $num -le $MAX ] 
do
  echo -n "$num "
  ((num++))
done
exit 0
