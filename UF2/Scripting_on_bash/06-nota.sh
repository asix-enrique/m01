#!/bin/bash
# @edt ASIX-M01 Curs 2023-2024
# 
# Febrer 2024
#
#  Validar nota: suspès, aprovat
#  	a) rep un argument
#  	b) és del 0 al 10
#---------------------------------
# 1)Validar Arguments 
ERR_NARGS=1
ERR_NOTA=2
if [ $# -ne 1 ]
then
  echo "Error: número de args incorrecte"
  echo "Usage: $1 nota"
  exit $ERR_NARGS
fi
# 2) Validar si es de [ 0 - 10 ]
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
  echo "Número $1 incorrecte"
  echo "Nota pren valors del 0 al 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi
# 3) Validar nota:
nota=$1
if [ $nota -lt 5 ] 
then 
  echo "Nota: $nota, has suspès"
elif [ $nota -lt 7 ] 
then
  echo "Nota: $nota, tens un aprovat"
elif [ $nota -lt 9 ]
then
  echo "Nota: $nota, tens un notable"
else 
  echo "Nota: $nota , tens un exel·lent"
fi
exit 0

