#!/bin/bash
#
# @edt ASIX M01
#
# for in mesos
# -----------------------

# 1) Validar num arguments
ERR_ARG=1
if [ $# -ne  1 ] 
then 
  echo "Error: numero args incorrecte"
  echo "Usage: $0 posar num mes " 
  exit $ERR_ARG 
fi

# 2) Validar per cada mes que el seu valor és del 1 al 12. 
# Si no ho és mostra un missatge d'error per stderr per continua processant la resta de mesos.

mes=$1
if ! [ $mes -ge 1 -a $mes -le 12 ]
then
  echo "Error: El numero de mes es incorrecte, ha de ser -->[1-12]" >> /dev/stderr
else
 case $mes in
 	1|3|5|7|9|10|12)
        	 echo "El mes: $mes te 31 díes"
	    	    ;;
    	2)
		    echo "El mes: $mes te 28-29 díes" 
		    ;;
   	 *)
		    echo "El mes: $mes te 30 díes"
		    ;;
	esac
fi
exit 0   
