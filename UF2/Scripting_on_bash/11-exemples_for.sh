#!/bin/bash
# @edt ASIX-M01 Curs 2023-2024
# 
# Febrer 2024
# Descripcio: exemples bucle for
#---------------------------------

#8)Llistar tots els logins numerats
llista_login=$(cut -d: -f1 /etc/passwd | sort)
num=1
for login in $llista_login
do
  echo "$num: $login"
  ((num++))
done
exit 0
#7) Llistar numerant les lineas

llistat=$(ls)
num=1
for elem in $llistat
do 
  echo "$num: $elem"
  ((num++))
done
exit 0  


# 6) Iterar pel resultat d'executar la ordre ls
llistat=$(ls)
for elem in $llistat
do 
  echo "$elem"
done
exit 0  
# 5) numerar arguments
num=1
for arg in $*
do 
  echo "$num: $arg"
  ((num++))
done
exit 0
# 4) $@ s'expandeix hi hagui o no cometes
for arg in "$@"
do 
  echo "$arg"
done
exit 0 

#3) Iterar per la llista d'arguments

for arg in "$*"
do 	
  echo "$arg"
done
exit 0
