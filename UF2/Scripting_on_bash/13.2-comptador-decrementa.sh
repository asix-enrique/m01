#!/bin/bash
#
# @edt Febrer 2024
#
# comptador decremant valor rebut
# -----------------

#1) validar arguments
if [ $# -ne 1 ]
then
  echo "Error: num args incorrecte"
  echo "Usage: $0 numero"
  exit 1
fi
#2) comptador decrement
MIN=0
num=$1
while [ $num -ge $MIN ]	
do
  	echo -n "$num "
	((num--))
done
exit 0
