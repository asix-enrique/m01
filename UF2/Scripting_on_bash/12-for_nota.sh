#!/bin/bash
# @edt ASIX M01
# Febrer 2024
#
# for in nota
# suspès, aprovat,notable,excel·lent
# ---------------------------------------

# 1) Validar numero arguments
#Cuando es mas de un argumento se puede hacer (-lt 1) o ( -eq 0 )

ERR_ARG=1
if [ $# -lt 1 ] 
then 
  echo "Error: numero args incorrecte"
  echo "Usage: $0 posar 1 o mes notes " 
  exit $ERR_ARG 
fi

# 2) Validar que nota = 0 a 10 i si no es correcte posar un missatge pero segeix comprobant la resta de notes.
# també dir quina qualificació tenen 
 
for nota in $@
do
  if ! [ $nota -ge 0 -a $nota -le 10 ]
  then
    echo "Error: nota $nota no valida, rang valid = (0-10)" >> /dev/stderr
   else
        if [ $nota -lt 5 ]
 	then 
   	  echo "La teva nota: $nota, has suspès"
 	elif [ $nota -lt 7 ]
	then
   	  echo "La teva nota: $nota , has aprovat"
 	elif [ $nota -lt 9 ]
	then
	  echo "La teva nota: $nota , tens un notable"
	else
	  echo "La teva nota: $nota, tens un exel·lent"
	fi
   fi
done
exit 0 
