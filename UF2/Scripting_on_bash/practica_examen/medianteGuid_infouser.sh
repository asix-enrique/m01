#!/bin/bash
#
#
#
#
# Rep per stdin GIDs i llista per stdout la informació de cada un d’aquests grups, en
# format: gname: GNAME, gid: GID, users: users
#----------------


# 1) Validar num args 

if [ $# -eq 0 ]; then
  echo "Error!! Num args incorrecte"
  echo "Usage: $0 Gid1 Gid2 "
  exit 1 

fi 


#2) 
for gid in $* 
do 
  usuarios=$(grep -E "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d ':' -f1,4 )
  echo "$usuarios" | sed -r "s/^([a-z]*):([0-9]*)$/gname: \1 gid:\2/"
done 
exit 0 
