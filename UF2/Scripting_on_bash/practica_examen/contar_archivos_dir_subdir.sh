#!/bin/bash 
#
# Enrique Lorente
#
#
# Escribe un script que recorra un directorio dado y cuente el número total de archivos en él, incluyendo los archivos en los subdirectorios.
#--------------------------------
#

#1) Validar argumentos 

if [ $# -ne 1 ]; then
  echo "Error! Num args incorrecte"
  echo "Usage: $0 directorio"
  exit 1
fi 


# 2) Validar que argumento es un directorio o no 
dir=$1
if [ ! -d $dir ]; then
  echo "Error: No es un direcorio"
  echo "Usage: $0 directorio"
  exit 2
fi 
# 3) Contar numero total de archivos en el direcorio y subdirectorios.  

find $dir -type f | wc -l 

exit 0 
