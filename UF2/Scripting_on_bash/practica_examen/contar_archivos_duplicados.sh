#!/bin/bash
#
#
# Copyright (c) 2024 Enrique Lorente. All Rights Reserved.
#
# Crea un script que encuentre los archivos duplicados en un directorio y sus subdirectorios. 
# Si hay duplicados, muestra los nombres de los archivos duplicados
#---------------------


# 1) Validar num args 

if [ $# -ne 1 ]; then
  echo "Error! Num args incorrecte"
  echo "Usage: $0 directorio"
  exit 1
fi

# 2) Validar que el argumento es un directorio 
dir=$1
if [ ! -d $dir ]; then
  echo "Error: No es un direcorio"
  echo "Usage: $0 directorio"
  exit 2
fi 
# 3) Comando para ver en los directorios y subdirectorios archivos duplicados

busqueda=$(find $dir -type f -exec md5sum {} + | sort | uniq -w 32 -d | cut -d ' ' -f3 )
echo "El archivo $busqueda esta duplicado"
