#!/bin/bash
# @ edt ASIX-M01 Curs 2023-2024
#
# $ prog file
# indcar si dir és: regular , dir , link 
# o altra cosa
#
#----------------------------------------

# 1) Validar rep 1 argument
ERR_NARGS=1 
if [ $# -ne 1 ]
then
  echo "Error: número de args incorrecte"
  echo "Usage: $1 dir or file"
  exit $ERR_NARGS
fi

# 2) Validar si: 

if !  [ -d $1 -o -f $1 -o -h $1 ]
then
  echo "Error: $1 no es dir or file"
  echo "Usage: $0 dir or file"
  exit $ERR_DIRECT
fi

tipus=$1
if [ -d $tipus ]
then 
  echo "$tipus , es un directori"
elif [ -f $tipus ]; then
  echo "$tipus , es un regular file"
elif [ -h $tipus ]; then
  echo "$tipus , es un symbolic link "
else 
  echo "$tipus , es un altra cosa"
fi
exit 0 


