#!/bin/bash
# Febrer 26-02-2024
# @edt ASIX M01 Enique Lorente
#
#
# 	a ) Rep un arg i és un directori i es llista
#	b ) Llistar numerant els elements del diri
#	c ) Per cada element dir si es dir, regultar file 
#	o altra cosa
#	d) 
# ------------------------
#

# 1) Validar num args
ERR_NARGS=1
if [ $# -eq 0 ]
then
  echo "Error: número de args incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# 2) Validar que cada argument es dir o no 
directoris=$*
for dir in $directoris; do
	if [ ! -d $dir ]; then
		echo "ERROR!: $dir no es un directori" >> /dev/stderr	
	else
		llista=$(ls $dir)
		echo "Llistat: $dir ----------"
		for elem in $llista; do 
			if [ -d $dir/$elem ]; then
				echo -e "\t $elem es un directori"
			elif [ -f $dir/$elem ]; then
				echo -e "\t $elem es un fitxer"
			else
				echo -e "\t $elem es un altra cosa" >> /dev/stderr
			fi 
		done
	
	fi
done
exit 0 
