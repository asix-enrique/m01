
## Patrons d'expressió regulars
    - Regex: Regular expressions 
    - Ordres : processor text
            · grep, cut , sort , tr , sed
            · head tail 

## Documentos/Ejercicios:
103.7-Exercicis.md

## Apunts: 
300-Regular expressions.pdf


& = and 
|| = or 
&& and
! not


# 1- GREP 

#### grep pattern file...
- -i ignore case (ignora si son mayusculas o minusculas)
- -v not match ( QUE NO tengan, (palabra, letra...))
- -w word (muestra todas las lineas por la (palabra/patron) que digas. )
- -n num (numera en que linea estan los patrones)
- -c count (como un wc -l)
- -A [N] after lines (linea después del patron indicado)
- -B [N] before lines(linea antes del patron indicado)
- -E extended
- -q no output
- -l list filename only

"" = permet expensions 
'' = literal el missatge 

grep = muestra todas las linieas que tengan el patron indicado

    
si creamos un alias grep='grep --color=auto' , 
lo que hace es mostrar el patron 
que hemos indicado en color



# 2- Basic regular expressions

-  [abcd]
- [char] 1 char from the charset (1 char de los indicados.) 
- ^ begin line (que comienze por.../principio de linea) (accent sircumflex )
- $ end line (final de linea)
- . 1 character (1 character el que sea)
-  _* 0-n occurrences precedent char 
(de 0 a n veces el character anterior)


grep "aa*" = a seguida de 0 o más a. 
echo "12345678A" | grep -E "^[0-9]{8}[A-Z]$"

# 3- Extendend regular expressions

#### PARA USARLAS NECESARIO
(===> egrep
===> grep -E)

- _+ 1 o més veces el character anterior
- _? 0 o 1 el character anterior
- {n}{n,}{,m}{n,m} 
- {3} = exactamente 3
- {3,} = 3 o més
- {,7} = nada o hasta a 7
- {3,7} = de 3 hasta 7
- | = or, acaba en 1 o 2 = grep -E "1$|2$" users
- _* = 0 o n vegades
- _{0,} = 0 a muchas 


### Ejercicios 
grep -E "user[0-9]?$" users = 0 o 1 digit numeric

grep -E "user[0-9]{2}$" users = exactamente 2 digit numerics

grep -E "^[0-9]{4}-[A-Z]{3}$" matricules  
#

# BASIC text filtres
- head
- tail
- expand/unexpand
- cut
- sort
- tr



## head

head -n -2 noms = muestra todas las lineas menos las 2 ultimas 


## tail 

tail -n +3 noms = muestra todas las lineas menos las 3 primeras

(wc = pot actuar per filtre o poden actuar directament 
  wc -l file 
  ordre | wc)


## cut 
Sempre que vulgui retallar columnes sempre s'ha de normalitzar (amb tr). 
el cut procesa sempre amb tab si no poses un delimitador 

cut -c 2-10 = retallar character del 2 a 10

cut -c 1,13- = retalla el character 1 i del 13 al final 

cut -c -25 = retalla fins el character 25


En /etc/passwd =
  Mostra el login, passwd, uid, gid , GECOS , homedir, shell

cut -f = retalla per camps

cut -d = delimita per el character que li posis per exemple (: , / |)

ls -l | cut -d' ' -f1,3,4 

blank = espais + tabs + combinació 



## sort 

sort -r = reverse 

sort -t = delimiter 

sort -k = key field

sort -n = ordena numericamente 

sort -k3nr = la k va antes de los r n o numeros de key field 

sort -t: -k4n -k1n oficinas

sort -t: -k1n oficinas

sort | uniq
- lexicographic order (default)
- -r reverse
- -n -g -H numeric order
- -t_ delimiter
- -knº key field
- -u (uniq)

## uniq 
```
uniq agrupa elements correlatius 


cut -d: -f3 dades | sort | uniq -c 

cut -d: -f3 dades | sort -u

cut -d: -f3 dades | sort | wc -l   
```




## MAN 

```
man 5 passwd = file formats and configuration 
man passwd = user commands 
```


### TR 

tr | normalize
- char to char (no text) "el tr fa chars NO string"
- delete,  -d 
- squeeze, -s (secuencia )
- normalize, 

"NO FILE ARGUMENT", només stdin  

fer tr _ _ _ file = no funciona 

Funciona =  ordre | tr
            tr < file 

Ejercicios ejemplo: 

tr ':' ' ' < noms 

tr ':' '\t' < noms  = pasa los dos puntos a tabulador

tail -n5 noms | tr ':' ','

echo "supersecretpassword" | tr 'aeiou' '43701' 

echo "supersecretpassword" | tr 'aeiou' '43701' 

echo "text in lowercase" | tr '[a-z]' '[A-Z]'


EJERCICIO MAL 
NO ES POT ESCRIURE Y SOBRE ESCRIURE EN UN FILE 
tr '[a-z]' '[A-Z]' < nom2 > nom2


# NORMALITZAR 

normalitzar blanks (espais i tabs de la separació de espais humans )

el separador no esta normalitzat 

[:blank:]
[:digit:] numeric
[:alpha:] alfabetic
[:alnum:] alfanumeric 
 
tr -s '[:blank:]' ':' < text

# Normalitzar els espais en blanc 

tr -s '[:blank:]' ' ' < fstab2 

normalitzar es posar un separador entre camps. 
normalment és necessari fero quan s'utilitzan blanks(espai, tabulador, salt de linea )

Quan es normalitza ha de ser sempre un caracter que no formi part del mateixa base de dades.


ls -l / | tr '[:blank:]' '\t' | cut -f2,3,5

ASCII 
els 32 primers caracters son de control.





# SED 

sed: editing text
- -n silent
- -i in place
- s search & replace
- d delete
- p print 
- g totes les substitucions de la linea 
- line
- start,end
- /regex/
- s/
- d/
- p/
- -r = versiones extended
- ^ accent circumflex  

sed '/patro/ s/vell/nou/g'  
sed 'i,f s/vell/nou/g'  
sed '/i/,/f/ s/vell/nou/g'  

sed 'fffff accio' 

sed 's/' = search and replace 

sed 's/patro_origen/patro_destí/_' fa només la primera substitució de la línea 
        vell          nou

sed 's/root/ROOT/g' passwd cambia todos los root que haya a ROOT

sed '1,4 d' noms = borrar las 4 primeras lineas y mostrar las restantes 


sed '4,$ d' noms = de la 4 al final , con el dolar indicamos que es hasta el final 


sed '1 s/e/XXXX/' noms = hacerlo solo a la linea 1 

sed '2,5 s/e/XXXX/' noms = hacerlo de la 2 a la 5
 

sed -r '/patro/ acció ' noms

patro = les lineas que contenen el patro. 

sed '/^[jJ]/ d' noms

sed -i 's/hola/HOLA/' noms = sustituirlo en el fichero , no en stdout 


echo "935550055" | sed -r 's/^([0-9]{2})/(\1)/' 
Es posa entre parentesis el que es vol memoritzar i es posa a cada numero 

echo "935550055" | sed -r 's/^([0-9]{2})([0-9]{3})/(\1) \2 /' 


sed 's/^.*//' passwd 


cut -d: -f1,3 passwd | sed -r 's/^(.*[a-z]):(.*[0-9]$)/\1(\2)'


25/01/2024

expansió de l'asterisc 
_* s'expandeix el més a la dreta possible 


### TRACTAMENT DE CAMPS 

buscar per: 
```
  - Primer camp:    ^____:  

  - Indicar un camp concret:    ^____:____:valor:

                          s'ha de fer així:
                            ^[^:]*:[^:]*:valor:

  - Ultim camp: :___$      sed -r 's/:[^:]*$/:123456789/' oficinas

  - Segon camp: sed -r 's/^([^:]*:)[^:]*:/\1--secret--:/' /etc/group

```


sed '/^11\t/ s/Este/Norte/' oficinas.dat 
sempre s'ha de posar els delimitadors

totes les lineas que comencin per 1 i un digit canvia Este a Nort de oficinas
sed '/^1[0-9]:/ s/Este/Norte/' oficinas
