# SCRIPTS

- Capçelera 
    - nom
    - data
    - sinopsis
    - descripció 

# 
2. Els scripts no els farem interactius 

3. msg | --> què ha passat i com fer-ho bé  

4. Fer un compte en git  
    .gitlab 


    #!/bin/bash  
    (shebang)  
- Serverix per saver on s'interpreta el script

Un script es recogir en un fitxer un conjunt d'ordres que podriem fer a la consola 

### executar un script  
- bash 01-exemple.sh 

### Conceptes basics
- var=valor , a les variables no s'ha de posar espais , el programa no funcionarà.

- '' = literal .  "" = es pot posar variables


----- 
``` bash
$# = nº arguments  
$0 = nombre del programa  
$1 =   
$2 =  
$3 =  
$$ = nº procés    
$@ = todos los argumentos    
```

   
# Apunts Condicionals scripts
#
## @edt ASIX 1r
## Enrique Lorente
#
### Condicions
if = condició 

then accions

elif condició
then 
	accions

else accions

fi.



### IF
#### Normes que no fer
```
1- No pot estar la mateixa instrucció a tots els casos.
2- No podem preguntar en les condicions allò que ja sabem.

```

```
Tota ordre retorna un valor de 'status', 'exit', 'de sortida' codi error.
variable $? = ens diu si l'ordre anterior a funcionat bé o malament

0 = ok, ha anat bé (igual a zero)
0 != error (diferent de zero)
```

### OPERADORS:
                - Relacionals =  >   ,    <  ,  =    , !=   , >=   , <=
                - Matematics =  '-gt', 'lt', '-eq', '-ne', '-ge', '-le'
                - Lògics = and, or ,not | -a, -o, !
                - Cadenes 
                - Fitxers


### Validar arguments

Sempre que hi ha un error  
    - msg error  
    - usage  
    - exit amb numero de error  

### Shift iterar per un argument en concret"entre comillas"
shift desplaza los argumentos para la izquierda  
aa bb cc --> shift   
bb cc --> shift  
cc   
  
### read -r line ( line = variable ) 
Procesa linea a linea la entrada estandard
 

### Possible error
s'ha de possar la ruta absoluta cuan hem de iterar per noms d'elements d'un llistat d'un directori. 




